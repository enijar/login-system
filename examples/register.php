<?php

require_once 'config.php';

?>
<!DOCTYPE html>

<html>
    <head>
        <script src="jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="style.css" type="text/css">
    </head>

    <body>
        <div id="ajax-result">
            <div class="hidden">&nbsp;</div>
        </div>

        <form id="register">
            <div class="spacing">
                <label for="username">Username</label>
                <input id="username" type="text" name="username">
            </div>

            <div class="spacing">
                <label for="email">Email Address</label>
                <input id="email" type="email" name="email_address">
            </div>

            <div class="spacing">
                <label for="password">Password</label>
                <input id="password" type="password" name="password">
            </div>

            <div class="spacing">
                <label for="password-repeat">Repeat Password</label>
                <input id="password-repeat" type="password" name="password_repeat">
            </div>

            <div class="spacing">
                <button id="register" type="submit">Register</button>
            </div>
        </form>

        <script>
            (function() {
                $(document).on('submit', '#register', function(event) {
                    event.preventDefault();

                    $.ajax({
                        type: 'POST',
                        url: 'register.post.php',
                        data: {
                            username: $('#username').val(),
                            email_address: $('#email').val(),
                            password: $('#password').val(),
                            password_repeat: $('#password-repeat').val()
                        },
                        success: function(data) {
                            var response = JSON.parse(data);

                            console.log(response);

                            $('#ajax-result').html('<div class="'+ response.type +'">'+ response.message +'</div>');
                        }
                    });
                });
            }());
        </script>
    </body>
</html>