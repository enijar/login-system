<?php

class UserLogin
{
    function query($sql, $values = []) {
        global $db;

        $stmt = $db->prepare($sql);
        $stmt->execute($values);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function hash_password($password)
    {
        $salt = bin2hex(openssl_random_pseudo_bytes(255));
        $hash = crypt($password, '$2a$11$' . $salt);

        $result['salt'] = $salt;
        $result['hash'] = $hash;

        return $result;
    }

    function check_username($username)
    {
        $result = $this->query("SELECT username FROM users WHERE LOWER(username) = ? LIMIT 1", [strtolower($username)]);

        if(strtolower($result['username']) === strtolower($username)) {
            return json_encode(['type' => 'error', 'message' => 'Username already exists']);
        }

        return true;
    }

    function check_email($email)
    {
        $valid_email = filter_var($email, FILTER_VALIDATE_EMAIL);

        if(empty($valid_email)) {
            return json_encode(['type' => 'error', 'message' => 'Email address is not valid']);
        }

        return true;
    }

    function check_password($password)
    {
        if(strlen($password) >= 8) {
            if(preg_match('/[0-9]/', $password)) {
                if(preg_match('/[A-Z]/', $password)) {
                    return true;
                } else {
                    return json_encode(['type' => 'error', 'message' => 'Password must container at least one capital letter']);
                }
            } else {
                return json_encode(['type' => 'error', 'message' => 'Password must contain at least one number']);
            }
        } else {
            return json_encode(['type' => 'error', 'message' => 'Password must be greater than 7 characters']);
        }
    }

    function register_user($username, $email, $password)
    {
        $email_check = $this->check_email($email);
        $username_check = $this->check_username($username);
        $password_check = $this->check_password($password);

        if($username_check === true) {
            if($email_check === true) {
                if($password_check === true) {
                    $password = $this->hash_password($password);
                    $salt = $password['salt'];
                    $hash = $password['hash'];

                    $this->query("INSERT INTO users (username, email_address, salt, hash, last_login, created_at, updated_at) VALUES (?, ?, ?, ?, NOW(), NOW(), NOW())", [$username, $email, $salt, $hash]);

                    return json_encode(['type' => 'success', 'message' => 'User is now registered']);
                } else {
                    return $password_check;
                }
            } else {
                return $email_check;
            }
        } else {
            return $username_check;
        }
    }

    function login_user($username, $password)
    {
        $result = $this->query("SELECT salt, hash FROM users WHERE username = ? LIMIT 1", [$username]);

        $salt = $result['salt'];
        $hash = $result['hash'];

        if(crypt($password, '$2a$11$' . $salt) === $hash) {
            $this->query("UPDATE users SET last_login = NOW() WHERE LOWER(username) = ?", [strtolower($username)]);

            return json_encode(['type' => 'success', 'message' => 'User login is correct']);
        } else {
            return json_encode(['type' => 'error', 'message' => 'Incorrect username or password']);
        }
    }
}