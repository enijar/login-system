# PHP Login System #

*Note: Still improving this, thanks Elliot for making me come back to this code.*

### Demo ###

* [Register user page](http://molly.local/snippets/login-system/register.php)
* [Login user page](http://molly.local/snippets/login-system/login.php)

### How do I get set up? ###

Download the necessary files. These are:

* config.php
* UserLogin.class.php
* register.post.php
* login.post.php

Create a database and in it create a table to hold all of your registered users. Below is SQL that will create the table and all the required columns for the login system to work.

### Database Schema ###

```
#!SQL

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

You can create your own register.php and login.php files to include extra or less user data. Just remember to update the relevant PHP files with your changes to the input fields.

### config.php ###

```
#!PHP

<?php

session_start();

$database = 'DATABASE_NAME';

$config = [
    'pdo' => [
        'dsn' => 'mysql:dbname=' . $database . ';host=localhost;charset=utf8',
        'username' => 'DATABASE_USERNAME',
        'password' => 'DATABASE_PASSWORD'
    ],
    'debug' => true
];

$db = new PDO($config['pdo']['dsn'], $config['pdo']['username'], $config['pdo']['password']);

require_once 'UserLogin.class.php';

$user = new UserLogin();

```

This project is optimised for AJAX POST requests to register.post.php and login.post.php, therefor all the responses you get back are in JSON format. In this example we user the **type** response to specify the class of the div we create and the **message** response for the text of that div.

### AJAX Example ###

```
#!JAVASCRIPT

(function() {
    $(document).on('submit', '#login', function(event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'login.post.php',
            data: {
                username: $('#username').val(),
                password: $('#password').val()
            },
            success: function(data) {
                var response = JSON.parse(data);

                console.log(response);

                $('#ajax-result').html('<div class="'+ response.type +'">'+ response.message +'</div>');
            }
        });
    });
}());

```

### register.post.php ###

```
#!PHP

<?php

require_once 'config.php';

if(!empty($_POST['username']) && !empty($_POST['email_address']) && !empty($_POST['password']) && !empty($_POST['password_repeat'])) {
    if($_POST['password'] === $_POST['password_repeat']) {
        echo $user->register_user($_POST['username'], $_POST['email_address'], $_POST['password']);
    } else {
        echo json_encode(['type' => 'error', 'message' => 'Passwords don\'t match']);
    }
} else {
    echo json_encode(['type' => 'error', 'message' => 'All fields are required']);
}

```

### login.post.php ###

```
#!PHP

<?php

require_once 'config.php';

if(!empty($_POST['username']) && !empty($_POST['password'])) {
    echo $user->login_user($_POST['username'], $_POST['password']);
} else {
    echo json_encode(['type' => 'error', 'message' => 'All fields requiresd']);
}

```


### Contribution guidelines ###

* Writing tests
* Code review
* Bug/issue reports

### Who do I talk to? ###

* Repo owner or admin
